#!/bin/bash
source $(dirname ${0})/ror-common
source $(dirname ${0})/ror-packages

set +e
[ -z "${1}" ] && echo "$(basename $0) <version>"
set -e

for p in ${P}; do
    cd ${PREDIR}/build-area
    debsign ${p}_${1}_amd64.changes
    dput ftp-master ${p}_${1}_amd64.changes
done
