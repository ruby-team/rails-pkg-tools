#!/bin/bash
source $(dirname ${0})/ror-common
source $(dirname ${0})/ror-packages

for p in ${P}; do
    cd ${PREDIR}/${p};
    git-buildpackage --git-tag --git-retag
done
