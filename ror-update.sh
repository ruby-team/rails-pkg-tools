#!/bin/bash
source $(dirname $0)/ror-common
source $(dirname $0)/ror-packages

[ -z "$1" ] && echo "$(basename $0) <version>"

for p in ${P}; do
    cd ${PREDIR}/${p};
    git co ${BRANCH}
    gbp-pull
    cv=$(dpkg-parsechangelog | sed -ne "s/^Version: \\(.*\\)-.*/\\1/p")
    g=$(echo ${p} | sed -ne "s/^ruby-\\(.*\\)-3.2/\\1/p")
    nv=${1}
    if dpkg --compare-versions "${nv}" gt "${cv}"; then

	# Download and tgzify gem
	TMPDIR=$(mktemp -d /tmp/pkg-ruby-extra.XXXXXXXX) || exit 1
	cd ${TMPDIR}
	gem fetch ${g} -v ${nv}
	gem2tgz ${g}-${nv}.gem
	cd $PREDIR/${p}
	orig=${p}_${nv}.orig.tar.gz
	mv $TMPDIR/${g}-${nv}.tar.gz ${PREDIR}/${orig}
	rm -rf ${TMPDIR}

	# Import new tgz
	git-import-orig --upstream-version=${nv} --no-interactive ${PREDIR}/${orig}
	# Update tight dependencies between RoR components
	./debian/rules clean
	# Commit debian/control only if it was updated (workaround for ruby-activesupport-3.2)
	if [ $(git diff debian/control | wc -l) -gt 0 ]; then
	    git add debian/control
	    git commit -m "Update debian/control to rails release ${nv}"
	fi
	# Create new changelog
	git dch -N ${nv}-1 -R -a
	# Commit changelog
	git commit -m "Prepare $(dpkg-parsechangelog | sed -ne "s/^Version: \\(.*\\)-.*/\\1/p") release" debian/changelog
    fi
    # Build package if the builder was specified on command line
    ${BUILDER}
done
